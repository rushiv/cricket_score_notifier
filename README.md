# Cricket Score Notifier

Script to display custom notifications via Growl or Ubuntu's notify-send for a running cricket match. The script uses Cricinfo's JSON feed of live matches.

## Config

You can configure to alert you on every run, boundaries & sixes or wickets. Configuration can be done in `config/app.yaml`

## Copyright

This script is not related to the IPL or ESPN Cricinfo

## License

This script is MIT licensed.
