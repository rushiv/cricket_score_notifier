require File.dirname(__FILE__) + "/cricket_score_notifier/version"
require File.dirname(__FILE__) + "/cricket_score_notifier/match"

require 'yaml'
require 'httpclient'
require 'json'

module CricketScoreNotifier

  @config = nil

  def self.load_config
    @config = YAML::load(File.open(File.dirname(__FILE__) + '/../config/app.yaml'))
    #puts @config.inspect
  end

  def self.run
    match_running = true
    status = nil
    client = HTTPClient.new()

    while match_running do
      url = "%s?r=%f" % [@config['json_url'], rand()]
      json = JSON.parse(client.get_content(url))
      if json.size == 0
        return false
      end

      # For now we just use one match json[1]
      # normally we would be search through and description and find the right match
      match = Match.new(json[0])
      match_running = match.is_running?
      # puts match.get_summary()

      level = 4
      sticky = false
      title = msg = ''

      if match.get_summary() != status
        last_ball = match.last_ball()
        runs = match.last_runs()

        puts match.get_summary()
        puts last_ball

        if not last_ball or last_ball.size == 0
          return false
        end

        # Setup the title & message to be displayed based on the
        # last ball delivered
        unless last_ball['dismissal'].empty?
          level = 1
          sticky = true
          title = last_ball['players'] + ', OUT'
          msg = "%s\n%s\n%s" % [last_ball['dismissal'], last_ball['text'], match.get_summary()]
        else
          msg = "%s, %s\n" % [last_ball['players'], last_ball['event']]

          unless last_ball['pre_text'].empty?
            msg += last_ball['pre_text'] + "\n"
          end

          unless last_ball['text'].empty?
            msg += last_ball['text'] + "\n"
          end

          title = match.get_summary()
          if runs >= 4
            level = 2
            title = "SHOT, %i runs - %s" % [runs, match.get_score()]
            msg += "\n" + match.get_summary()
          elsif runs >= 1
            level = 3
          end
        end

        if @config['notify_level'] >= level
          puts "\nDo Growl Notification!\n", title, msg
        elsif not last_ball['pre_text'].empty?
          puts "\npre_text growl\n", last_ball['pre_text']
        end

        status = match.get_summary()
      end

      sleep(@config['sleep_interval'])
    end

    puts "Script complete, Good Bye!"
  end

  def self.init
    load_config
    run
  end

  def _clean(text)
    return text.gsub(/<\/?[^>]*>/, "").strip()
  end
end
